+++
title = "LED Spy 2.0"
[data]
baseChartOn = 3
colors = ["#627c62", "#11819b", "#ef7f1a", "#4e1154"]
columnTitles = ["Section", "Status", "Author"]
fileLink = "content/projects.csv"
title = "Projects"

+++

{{< block "grid-2" >}}
{{< column >}}

# Take control of your lighting.

`LED Spy 2.0` is the next logical evolution of the `LED Spy Project`, offering fine grain control of your arcade controller's lighting, with an easy to use [graphical designer](todo_add_link).

the `LED Spy Project` started the summer of 2019, and has been constantly evolving since.

{{< tip "warning" >}}
Feel free to raise a [designer issue](https://gitlab.com/luberryscc/led-spy/designer/-/issues/new "Open a LED Spy Designer Gitlab Issue")(s), a [firmware issue](https://gitlab.com/luberryscc/led-spy/firmware/-/issues/new "Open a LED Spy Firmware Gitlab Issue")(s) or request new feature(s). {{< /tip >}}
{{< tip >}}
This documentation is a work in progress, and is written by a single individual. If you wish to contribute to the documentation open a [merge request](https://gitlab.com/luberryscc/led-spy/documentation/-/merge_requests/new) with your changes.
{{< /tip >}}
{{< tip >}}

# Now Open Source

As I have moved on from development of this project I have decided to release the source code.

- [Firmware](https://gitlab.com/luberryscc/led-spy-samd)
- [Communication Protocol Buffers](https://gitlab.com/luberryscc/led-spy-protos)
- [Designer](https://gitlab.com/luberryscc/led-spy-designer-2)
- [Hardware](https://gitlab.com/luberryscc/led-spy-samd-hardware)
  {{< /tip >}}

{{< button "docs/" "Read the Docs" >}}{{< button "https://gitlab.com/luberryscc/led-spy/designer/-/releases" "Download Designer" >}}
{{< /column >}}

{{< column >}}
![designer](/images/designer/features/designer.png)
{{< /column >}}
{{< /block >}}
