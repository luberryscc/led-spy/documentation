---
title: "LED Spy Docs"
weight: 1
---

These docs consists of two parts:

1. LED Spy Installation
2. LED Spy Designer

Please note that the features listed under each theme are independent of each other. That is to say, some features may only be found in one theme and not in both.

{{< button "./install/" "LED Spy Installation Docs" "mb-1" >}}

{{< button "./designer/" "LED Spy Designer Docs" >}}
