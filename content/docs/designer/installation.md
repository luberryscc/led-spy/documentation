---
title: Installation
weight: 3
---

## Windows

Installation on windows is as simple as downloading and running the the automated installer.

1. Download the latest `LEDSpyDesigner-2.0.0Setup.exe` from the [releases](https://gitlab.com/luberryscc/led-spy/designer/-/releases) page.

2. Run the downloaded setup file.
   **_Note: Windows may warn you about installing files from an unknown publisher, this is normal. select the option to run anyway_**

3. Click the install button in the setup program. This will install the designer and required drivers for firmware upgrades.

![installer windows 1](/images/designer/install/windows-install-1.png)

4. The program should only take a few seconds to install. Then click the finish button.

![installer windows 2](/images/designer/install/windows-install-2.png)

## Linux

Linux should be quite easy to use, and you should just be able to run the appimage after making it executeable.
For more in depth documentation on using appimages, head over to the [official documentation](https://docs.appimage.org/user-guide/index.html).

```sh
chmod +x LED_Spy_Designer-x.x.x-x86_64.AppImage
```
