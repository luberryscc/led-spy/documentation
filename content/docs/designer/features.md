---
title: Features
Weight: 2
---

![designer](/images/designer/features/designer.png)

- Per button lighting customization
  - reactive lighting
  - breathing animation
- Multiple Profiles (depends on device support. hotkey profiles coming soon)
  ![inputdisplay](/images/designer/config/InputSwitch.gif)
- Integrated input display
  ![inputdisplay](/images/designer/config/InputDisplay.gif)
- Support for multiple controller types, including stickless and standard fight sticks
