---
Title: LED Spy Designer
weight: 1
---

LED Spy Designer 2 is a lighting configuration tool, and input display software for controllers equipped with the LED Spy 2 addon PCB.

{{< button "./installation/" "Get started with LED Spy Designer 2" >}}
