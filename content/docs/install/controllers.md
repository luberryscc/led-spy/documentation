---
Title: Controllers
weight: 23
---

## Hit Box Arcade Controllers

- [Smash Box](#smash_box)
- [Hit Box](#hit_box)

---

## Other Controllers

- [Bit Bang Gaming](#bit_bang_gaming)
- [Brook/Generic](#brook)

---

## Smash Box

### Tools {#sbxtools}

- PH1 Screwdriver `GEN 3, and some GEN2`
- 2.5MM Hex `GEN 2 only`
- Cordless Drill [^1]
- Various Drill Bits [^1]
- Center Punch, [this one](https://www.amazon.com/gp/product/B008DXYOLC/) is what i use. [^1]
- Rotary Tool and [carbide burrs](https://www.amazon.com/gp/product/B01MXURLTG) (optional) [^1] [^2]
- Hardened steel needle files (preferably not diamond as these clog easily) [^1]
- Safety Glasses (for use when drilling/using the rotary tool)
- Needle Nose Pliers (can make wire removal easier)
- Button Popout tool (optional, but it makes it way easier to remove buttons). I recommend [this one](https://paradisearcadeshop.com/collections/accessories-parts/products/paradise-arcade-shop-button-popouts) from Paradise Arcade Shop [^3]

### Materials {#sbxmaterials}

- 1x [LCC LED Spy 2.0 Board](http://www.luberryscc.com/product/led-spy-2-0)
- 1x [LCC LED Spy Serial Breakout](http://www.luberryscc.com/product/smash-box-gen-2-serial-breakout) `GEN 1-2 only`
- 23x [LCC 5050 LED Modules](http://www.luberryscc.com/product/lcc-5050-rgb-led-module) or other neopixel/ws2812 led options that supply 2x LEDs per actuator. (optional) [^3]
- 20x [Paradise Kaimana J2 3" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-3-wire-harness) (optional) [^3]
- 2x [Paradise Kaimana J2 6" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-6-wire-harness) (optional) [^3]
- 1x [Paradise Kaimana J2 8.5" Harness](https://paradisearcadeshop.com/products/paradise-kaimana-j2-85-wire-harness) (optional) 9" also works [^3]
- 1x [LCC LED Spy Hub Kit](http://www.luberryscc.com/product/led-spy-hub)
- 1x [LCC LED SPY Battery Bank Wiring Kit](http://www.luberryscc.com/product/led-spy-battery-bank-wiring-kit) (optional) [^3]
- 1x always on [^4] battery bank, my recommendations are below.
  - [Voltaic Systems V50](https://voltaicsystems.com/v50/) (12800mAh)
  - [Voltaic Systems V25](https://voltaicsystems.com/v25/) (6400mAh)
- 1x USB power cable to run your battery bank, if you use the voltaic ones, an A-C will suffice. Just needs to be long enough to reach from the LED Spy Hub to the battery bank.
- 23x 24mm Transparent/Clear buttons [^3]
- Way to fasten LEDS [^3]
  - Sanwa Buttons - the best way to fasten sanwa buttons is the [LCC Sanwa LED Module Mounting Clip](https://www.luberryscc.com/product/sanwa-led-module-mounting-clip)
  - Seimitsu Buttons - the LCC modules fit snugly around the seimitsu switch so no mounting mechanism is required.
  - Crowns/Gamerfingers/Other - a couple small dabs of hot glue are your friend in this instance as i have yet to make a reliable clip design for my modules
- Masking/painters tape used to protect the paint on the surface during cutting [^3]
- Some way to mark wires (optional, but recommended)
  - pen and masking/painters tape work wonders
- Magnet or cup to keep screws together (optional, but recommended)
- 3M VHB (Very High Bond) Tape (for mounting the battery bank) [^3]

### Disassembly {#sbxdisassembly}

{{<tip "Info">}}
Up until `Step 7` you can follow the [official guide](https://www.hitboxarcade.com/blogs/support/replacing-buttons-and-acrylic{{<hba-affiliate>}}) for disassembly, as it goes much more in depth than this guide.
{{</tip>}}

1. Place your controller onto its face, try and use a soft surface to avoid scratching it.
2. Remove the 6x Phillips Screws (`Gen 3`), the 4 phillips and 2 hex screws (`more recent GEN 2 Models`), or the 4 rubber feet and 2 hex screws (`older GEN 2 Models`) and remove the base plate. If you are not installing LEDs you can skip to [LED Spy Hub Installation]({{<ref "#sbxhubinstall">}}).
   {{<tip "Info">}}
   if you have a gen 3 base plate is a good surface to place the controller on top of since it is padded with foam.
   {{</tip>}}
3. Now is when you should label the wires, this is optional, but will help you later on. You may also take a picture of the wiring. The easiest way to label the wires is use a piece of masking tape around each pair of wires. and write unique labels on each.
4. Now it is time to remove the wires. There are 2 variants of connector used on the Smash Box, Gen 2 and prior used vertical style connector, whereas Gen 3 currently uses right angle connectors. The right angle connectors are significantly easier to remove than the vertical style.

   - **Horizontal Removal**:

     There are 2 main methods for removal of right angle/horizontal quick disconnects.

     - **Method 1**: Use your fingers as demonstrated by the below gif, courtesy of Hit Box Arcade LLC.
       {{<gfycat "AggressiveWindyDodo" >}}
     - **Method 2**: Use your needle nose pliers while holding the controller down with your other hand as demonstrated by the below gif, courtesy of Hit Box Arcade LLC.
       {{<gfycat "eminentcleverhake" >}}

   - **Vertical Removal**:

     Since the vertical quick disconects are on super tight, you will want to hold the controller down with one hand, grab the disconnect with the needle nose in the position noted below, and pull straight up.
     {{<figure src="/images/install/WHERE_TO_GRAB_IT.jpg" attr="Image courtesy of Hit Box Arcade LLC">}}

5. Remove the buttons from the controller, either using the button pop out tool mentioned earlier, or by squeezing the tabs on the buttons with your fingers.
6. Next you need to remove the plastic rivets from the controller, this can be done by pressing on the center pin from the back side with a screwdriver. They should pop out pretty easily this way. Place the rivets somewhere safe as you will need them to reinstall the plexi later.
   {{<figure src="/images/install/rivet_remove.png" attr="Image courtesy of Hit Box Arcade LLC">}}
7. Remove the plexi and set it aside somewhere it won't be scratched (especially if you plan to reuse it)
8. Now remove all connectors for the side ports, and switches.
   {{<tip>}}
   Take note of the connector locations. You may want to label these with some tape
   {{</tip>}}
   The switch connectors can usually be removed by hand, but if not, follow a process similar to the `Vertical` quick disconnect removal from `step 4`. The connectors to remove are marked below with a purple arrow.
   ![sideportcables](/images/install/sbx/side-port-cables.png)
9. Now remove the 4 screws for the side ports denoted below by the purple arrows. If you have a `Gen 2` Smash Box, you must also remove the 3 H 2.5 screws on the side as denoted by the white arrows.
   ![sideportscrews](/images/install/sbx/side-port-screws.png)
10. You may now remove the side ports and set them aside.
11. Now you must remove the main board. Gen 2 and 3 differ so follow the correct steps for the controller you have.

- **GEN 3**:
  To remove the screws from the gen 3 board remove the 5 screws indicated by the purple arrows in the below image. You should now be able to lift the main board out and set it somewhere safe.
  ![gen3pcbscrews](/images/install/sbx/gen3-pcb-screws.png)
- **GEN 2**:
  The `Gen 2` Smash Box has screws that are not accessible until you take the top green breakout board off, so it requires multiple steps.
  1. Remove the 4 main pcb screws pictured below (there is one under that bunch of cables, trust me)
     ![gen2pcbscrews](/images/install/sbx/gen2-pcb-screws.jpg)
  2. Gently lift the green pcb off of the blue one underneath. It may be a bit tight, but try to apply even pressure to remove it.
  3. With the board off, there should be 4 more screws holding in the blue pcb. Remove those, and lift it out.

### Power Switch Installation {#sbxpwrinstall}

Now it is time to prepare to cut the hole for the LED power switch.

1. First, to prevent scratching the case you should wrap the edges in some sort of masking/painters tape, this will also let us easily mark our hole for our switch as shown below. This guide will assume you are using the switch supplied with the wiring kit we sell, as it is a close match to the switches already installed. You want to mark out the rough location to give you a guide when cutting the hole, and try and line it up with the existing switches.
   ![switchmarking](/images/install/sbx/SwitchMarking.jpg)
2. Now, take your center punch and mark a spot in the center of where you plan to install your switch.
3. Drill a hole where the center punch mark was made that is slightly smaller than the switch (give your self some room to adjust)
4. Now, with the hole drilled, use either your files to make the hole rectangular, or rough it out with the dremel and burrs.
5. Gradually fit the hole to the switch, making sure to check the fit. You want the switch to fit snugly.

### Battery Bank Installation {#sbxbatinstall}

Now to install the battery. This is easier prior to full reassembly, as plugging in the cables can be difficult with the main pcb installed.

1. First you will need to use some VHB tape to secure the battery similar to the figure below. you can use more than pictured to secure it better.
   {{<tip>}}
   Make sure to clean both the battery and inside of the case with some sort of degreaser, or isopropyl alcohol to promote better adhesion
   {{</tip>}}
   ![batterytape](/images/install/sbx/battery.jpg)
2. Peel the backing from the tape on the battery and stick it down in the position shown. Make sure you will still have access to the charge port and one output port for wiring.
   ![batteryinstall](/images/install/sbx/battery-installed.png)
3. Now install the battery hold down block that came with the battery installation kit. first remove the red film from the back side of the block.
   ![batteryretainer](/images/install/sbx/battery-hold-down-tape.jpg)
4. Next stick the hold down block to the battery. This will help prevent the battery from falling down over time by pressing against the bottom plate.
   ![batteryretainerinstall](/images/install/sbx/battery-hold-down-installed.jpg)
5. Next wire the battery harness to the power switch. If you purchased the wiring kit you should have received a cable similar to the one below.
   ![batterycable](/images/install/sbx/battery-harness.jpg)
   {{<tip>}}
   When wired to the switch, you may need to bend the leads a bit to clear the controller main board as shown below.
   ![switchcables](/images/install/sbx/power-switch-cables.jpg)
   {{</tip>}}
6. Now we plug the USB-A end of the harness into the usb port closest to the outside of the case as shown.
   ![batteryusba](/images/install/sbx/battery-usba.png)
7. Assuming you have the voltaic battery. Plug in the charge cable type c port into the battery bank. we will route this later.

### Reassembly (Part 1) {#sbxreassembly1}

Now it is time to begin partially reassembling your controller, you can either follow my below steps, or the [official guide](https://www.hitboxarcade.com/blogs/support/replacing-buttons-and-acrylic{{<hba-affiliate>}}) through step `6B`.

1. First, reinstall your plexi, and the rivets. And press them in until it clicks, as pictured below.
   {{<figure src="/images/install/rivet_install.png" attr="Image courtesy of Hit Box Arcade LLC">}}
2. Now insert your buttons, you may need to adjust their positioning later to fit the leds. Make sure they fully snap into place.
3. Reinstall the side ports and screws. (the reverse of [Disassembly]({{<ref "#sbxdisassembly">}}) `step 9`)
4. Reinstall the main board. (the reverse of [Disassembly]({{<ref "#sbxdisassembly">}}) `step 10`)
5. Reinstall all side port cables except for the usb port one, we will come back to that later. (the reverse of [Disassembly]({{<ref "#sbxdisassembly">}}) `step 8`)

### LED Installation {#sbxledinstall}

The next step is to install the LED Modules, this guide will assume you are using the `LCC 5050 LED Modules`.

1. First position all of your modules so that they don't interfere with eachother. you may have to rotate the buttons some to get them all to fit. (its a bit of a puzzle).
   ![leds](/images/install/leds.jpg)
2. Now either secure the modules with some hot glue, or my LED clips (for sanwas) making sure the wires quick disconnects will still plug in.
3. Next we need to connect the leds. There are two different connectors on the leds. The 3 pin is the input, and the 4 pin is the output. Route your cables as you see fit as you can always remap the positon in software, however you want the input to be pretty close to the main pcb. Below is probably the optimal routing.
   {{<tip>}}
   All connections except for the following use the `3"` cables:
   | Connection | Length |
   | -------------- | ------ |
   | LED Spy -> 0 | 6" |
   | 10 -> 11 | 6" |
   | 21 -> 22 | 8.5" or 9"|
   {{</tip>}}
   ![wiring](/images/install/sbx/ledroute.jpg)
4. Once the leds are wired, reconnect all of the quick disconnects to the correct buttons.

### LED Spy Hub Installation {#sbxhubinstall}

Now it is time to install the `LED Spy Hub` which lets us reuse the side usb port for both battery bank charging, as well as for led spy and Smash Box programming.

1. First we need to install the LED spy Hub. For both `Gen 2` and `Gen 3` we should be able to mount the hub in roughly the same spot. To begin we want to remove the screw marked in the image below. On the left is the `Gen 2` and right is `Gen 3`.
   ![hubscrew](/images/install/sbx/screw4hub.jpg)
2. Now mount the hub as shown below.
   ![hubinstall](/images/install/sbx/hubinstall.jpg)
3. Now, take the plug meant to go into the usb side port and plug it into the matching 4 pin header on the hub labeled `JSBX1`
4. Take the hub's included 5 pin JST cable and plug one end into the hub, leave the other end be, we will come back to this later.
5. Take the 4 pin cable attached to the hub and plug it into the side port USB.
6. If you are running LEDS, Take the USB A end of your battery bank charge cable you installed earlier and plug it into the USB A port on the hub.

### LED Spy Board Installation {#sbxspyinstall}

Now the `LED Spy Board` can be installed.

1. First plug the included 4 pin JST cable into the port labelled JEXT1 on the LED Spy PCB as pictured below.
   ![extcable](/images/install/led-spy-ext-cable.jpg)

2. Next you must mount the PCB. Here we have another instance of `Gen 2` and `Gen 3` differing.
   - `Gen 2`: For `Gen 2` you will need to mount it to the body of the controller with 3mm adhesive standoffs which you can pick up from our friends over at Bit Bang Gaming [here](https://bitbanggaming.com/collections/pcb-feet/products/snap-in-adhesive-pcb-feet-3mm-hole-4-pack). Below is the rough positioning as I don't have an available `Gen 2` Smash Box for mockup.
     ![gen2pos](/images/install/sbx/gen2boardpos.jpg)
   - `Gen 3`: For `Gen 3` you can either mount it like you did above for gen 2, or you can make use of the bottom left screw hole on the `Gen 3` PCB as shown. Make sure to place a plastic washer between the two boards and mount it as as shown below.
     ![gen3pos](/images/install/sbx/gen3mount.jpg)
3. Now we can plug LED Spy board into the Smash Box PCB, again this step is slightly different between `Gen 2` and `Gen 3`

   - `Gen 2`: There are a couple of steps for this variant listed below.

     1. First we must install the `LED Spy Serial Breakout` as pictured below.
        ![serialbreakout](/images/install/sbx/serialbreakout.jpg)
     2. Now plug the free hanging end of the 4 pin JST we plugged into the `LED Spy` board earlier into the `JEXT1` port of the breakout

   - `Gen 3` : Gen 3 should just be mostly plug and play. You should just need to plug the other 4 pin connector from the board connector into the `JEXT1` port
     {{<tip>}} There is a minor connector difference on some gen 3 boards, and you may need to trim the connector as pictured in the popout.{{</tip>}}
     ![gen3ext](/images/install/sbx/gen3ext.jpg)

4. Now take the other 5 pin connector that we plugged into the hub earlier and plug it into the 5 pin on the LED Spy board labelled `JUSB1`. If you do not plan to install leds you can proceed to [Reassembly (Part 2)]({{<ref "#sbxreassembly2">}}) section.

5. Assuming you are installing LEDs, take the 4 pin connector on the power harness connected to the battery and plug it into the port labelled `JPWR1`
6. Now take the first LED cable in the chain and plug it into the `JLED1` port on the led spy board.

### Reassembly (Part 2) {#sbxreassembly2}

1. Now that everything should be installed, we can replace the bottom panel and the 6 screws.

2. Now you may proceed to the [LED Spy Designer]({{<ref "/docs/designer">}}) documentation and follow the instructions there to set up your lighting, or use input display

---

## Hit Box {#hit_box}

### Daughterboard Info {#hbxdbinf}

Below is a picture of the `Hit Box Daughterboard for LED Spy` which is a board meant to be placed between the breakout, and the green brook pcb in the controller.
![dbpic](/images/install/hbx/hbls.png)

### Tools {#hbxtools}

- PH1 Screwdriver `Post June 2020`
- 2.5MM Hex `Pre June 2020`
- Cordless Drill
- Various Drill Bits
- Center Punch, [this one](https://www.amazon.com/gp/product/B008DXYOLC/) is what i use.
- Safety Glasses (for use when drilling)
- Needle Nose Pliers (can make wire removal easier)
- Button Popout tool (optional, but it makes it way easier to remove buttons). I recommend [this one](https://paradisearcadeshop.com/collections/accessories-parts/products/paradise-arcade-shop-button-popouts) from Paradise Arcade Shop [^3]

### Materials {#hbxmaterials}

- 1x [LCC LED Spy 2.0 Board](http://www.luberryscc.com/product/led-spy-2-0)
- 1x [LCC Hit Box Daughterboard for LED Spy](http://www.luberryscc.com/product/hit-box-daughterboard-for-led-spy)
- 1x USB Passthrough
  - we sell a usb wiring kit that is plug and play with LED Spy [here](https://www.luberryscc.com/product/led-spy-usb-wiring-kit) that is based off of the Neutrik mounting pattern
- 16x [LCC 5050 LED Modules](http://www.luberryscc.com/product/lcc-5050-rgb-led-module) or other neopixel/ws2812 led options that supply 2x LEDs per actuator. (optional) [^3]
- 15x [Paradise Kaimana J2 3" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-3-wire-harness) (optional) [^3]
- 1x [Paradise Kaimana J2 6" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-6-wire-harness) (optional) [^3]
- 15x 24mm Transparent/Clear buttons [^3]
- 1x 30mm Transparent/Clear buttons [^3]
- Way to fasten LEDS [^3]
  - Sanwa Buttons - the best way to fasten sanwa buttons is the [LCC Sanwa LED Module Mounting Clip](https://www.luberryscc.com/product/sanwa-led-module-mounting-clip)
  - Seimitsu Buttons - the LCC modules fit snugly around the seimitsu switch so no mounting mechanism is required.
  - Crowns/Gamerfingers/Other - a couple small dabs of hot glue are your friend in this instance as i have yet to make a reliable clip design for my modules
- Masking/painters tape used to protect the paint on the surface during cutting [^3]
- Some way to mark wires (optional, but recommended)
  - pen and masking/painters tape work wonders
- Magnet or cup to keep screws together (optional, but recommended)
- [3mm Adhesive Standoffs](https://bitbanggaming.com/collections/pcb-feet/products/snap-in-adhesive-pcb-feet-3mm-hole-4-pack).

### Disassembly {#hbxdisassembly}

{{<tip "Info">}}
Up until `Step 7` you can follow the [official guide](https://www.hitboxarcade.com/blogs/support/replacing-buttons-and-acrylic{{<hba-affiliate>}}) for disassembly, as it goes much more in depth than this guide.
{{</tip>}}

1. Place your controller onto its face, try and use a soft surface to avoid scratching it.
2. Remove the 6x Phillips Screws (`Post June 2020`), the 4 phillips and 2 hex screws (`later Pre June 2020 models`), or the 4 rubber feet and 2 hex screws (`older models`) and remove the base plate.
   {{<tip "Info">}}
   if you have a gen 3 base plate is a good surface to place the controller on top of since it is padded with foam.
   {{</tip>}}
3. Now, if your controller has a

4. Now is when you should label the wires, this is optional, but will help you later on. You may also take a picture of the wiring. The easiest way to label the wires is use a piece of masking tape around each pair of wires. and write unique labels on each.
5. Now it is time to remove the wires. There are 2 variants of connector used on the Hit Box, `Pre June 2020` used vertical style connector, whereas `Post June 2020` currently uses right angle connectors. The right angle connectors are significantly easier to remove than the vertical style.

   - **Horizontal Removal**:

     There are 2 main methods for removal of right angle/horizontal quick disconnects.

     - **Method 1**: Use your fingers as demonstrated by the below gif, courtesy of Hit Box Arcade LLC.
       {{<gfycat "AggressiveWindyDodo" >}}
     - **Method 2**: Use your needle nose pliers while holding the controller down with your other hand as demonstrated by the below gif, courtesy of Hit Box Arcade LLC.
       {{<gfycat "eminentcleverhake" >}}

   - **Vertical Removal**:

     Since the vertical quick disconects are on super tight, you will want to hold the controller down with one hand, grab the disconnect with the needle nose in the position noted below, and pull straight up.
     {{<figure src="/images/install/WHERE_TO_GRAB_IT.jpg" attr="Image courtesy of Hit Box Arcade LLC">}}

6. Remove the buttons from the controller, either using the button pop out tool mentioned earlier, or by squeezing the tabs on the buttons with your fingers.
7. Next you need to remove the plastic rivets from the controller, this can be done by pressing on the center pin from the back side with a screwdriver. They should pop out pretty easily this way. Place the rivets somewhere safe as you will need them to reinstall the plexi later.
   {{<figure src="/images/install/rivet_remove.png" attr="Image courtesy of Hit Box Arcade LLC">}}
8. Remove the plexi and set it aside somewhere it won't be scratched (especially if you plan to reuse it)

{{<tip>}}
Note:
While not required, its better to remove the main pcb, as we want to reduce the chance of damaging it when we proceed to drill into the case for USB Port mounting later on.
{{</tip>}}

9. Now you must remove the usb plug from the main board. It should be a white 5 pin connector as pictured below.
   ![hbusb](/images/install/hbx/usb.jpg)
10. Now remove the 5 phillips screws as indicated below, and remove the main pcb. We can now set this aside for later.
    ![hbscrews](/images/install/hbx/screws.jpg)

### Side USB Install

Now that we are down to a mostly bare metal case, we can now mount our side usb port for the LED Spy board.
{{<tip>}}
This guide assumes you are using either our wiring kit, or another neutrik compatible port. Here is a <a href="/templates/neutrik24.pdf" target="_blank">template</a>.
{{</tip>}}

1. Wrap the edges of your case in masking/painters tape to protect the case from being scratched. You may also want to tape the cable for the usb port out of the way as well.
2. Pick a spot for your usb port. I like to position the port roughly in the spot pictured below.
   ![portloc](/images/install/hbx/sideportloc.jpg)
3. Print out the <a href="/templates/neutrik24.pdf" target="_blank">template</a>, and trim it to be a small rectangle, and tape it to the side of the box where you wish to put the port.
4. Now you can use your center punch to mark the centers of the 3 holes you wish to drill.
5. Drill pilot holes in all 3 spots with a 3mm or 1/8" drill bit (this is the final size for the two mounting holes)
6. Now take a 24mm hole saw or step bit and drill out a 24mm hole in the side of the case.
7. Remove the tape from the controller now and clean up all the filings.
8. Use a deburring tool or some sandpaper to clean up the hole.
9. Now you can screw in your usb port.

### LED Spy Board Daughterboard Installation {#hbxlsdbinstall}

1. Take the mainboard you removed earlier and use an exacto knife or a small flathead to remove the adhesive from the connectors that helps secure the main pcb. It should come off with some light scraping.
   ![adhesivescrape](/images/install/hbx/scrape.jpg)
2. Gently pull the two boards apart. You may need to slowly rock it side to side. You should now be left with two separate pcbs.
   ![separatepcbs](/images/install/hbx/separatepcbs.jpg)
3. Now, install the `Hit Box Daughterboard for LED Spy` into the larger breakout board as pictured below.
   ![dbinstall](/images/install/hbx/daughterboardinst.jpg)
4. Now, because of how the brook on the Hit Box was assembled, we it is a good idea to remove some of the plastic in the center of the pictured connectors to relieve stress on the joint, as shown below.
   ![jointstress](/images/install/hbx/jointstress.jpg)
5. Now insert the brook into the socket on the daughterboard as shown below.
   ![insertbrook](/images/install/hbx/insertbrook.jpg)
6. Now take the included 8" zip tie and use it to secure the stack together. This will help prevent vibrations from shaking any of the boards loose.
   ![ziptie](/images/install/hbx/ziptie.jpg)

7. Reinstall your main pcb stack now following the reverse of steps `9 and 10` of [Disassembly](#hbxdisassembly). It should look like the image below.
   ![reassemble](/images/install/hbx/hbxpcbinst.jpg)
8. You may now reinstall your buttons as well, but do not plug in the cables yet.

### LED Spy Board Installation {#brookspyinstall}

Now the `LED Spy Board` can be installed.

1. First find a suitable spot to mount your board in your case there alot of open space towards the center of the case next to the main pcb.
2. Plug the included 4 pin JST cable into the port labelled JDB1 on the LED Spy PCB as pictured below.
   ![extcable](/images/install/jdb1.jpg)

3. Next you must mount the PCB, the easiest way is to use the 3mm standoffs mentioned in materials.
4. Now plug the other end of the 4 pin JST cable into the daughterboard you installed earlier.

5. Now connect the LED Spy Board to a usb passthrough either through the 5 pin `JUSB1` or the micro USB Port labeled `JUSB2`
   ![ls2](/images/led-spy-2.png)

6. You should now have something similar to below (except the LED Spy board would be secured)
   ![ledspyinstalled](/images/install/hbx/lsinstalled.jpg)

{{<tip>}}
If you do not wish to install LEDs, you may skip to [Reassembly](#hbxasm).
{{</tip>}}

### LED Installation {#hbxledinst}

The next step is to install the LED Modules, this guide will assume you are using the `LCC 5050 LED Modules`.

1. Position all of your modules so that they don't interfere with eachother. you may have to rotate the buttons some to get them all to fit. (its a bit of a puzzle).
   ![leds](/images/install/leds.jpg)
2. Now either secure the modules with some hot glue, or my LED clips (for sanwas) making sure the wires quick disconnects will still plug in.
3. Next we need to connect the leds. There are two different connectors on the leds. The 3 pin is the input, and the 4 pin is the output. Route your cables as you see fit as you can always remap the positon in software, however you want the input to be pretty close to the main pcb. Below is probably the optimal layout, or close to it.
   ![wiring](/images/install/hbx/layout.jpg)
4. Now, make sure you install the included jumper on the `LED 5V` pins to allow passthrough usb power for the leds.
5. Now plug the first led in the chain into the `JLED1` port of the LED Spy board.

### Reassembly {#hbxasm}

1. Reattach all of your quick disconnect cables to their respective buttons.
2. Reinstall the bottom plate.
3. Now once you reassemble your controller you may proceed to the [LED Spy Designer]({{<ref "/docs/designer">}}) documentation and follow the instructions there to set up your lighting, or use input display

## Brook/Generic {#brook}

### Tools {#brooktools}

{{<tip>}}
This list is just a generic suggestion, and is not an exhaustive list as it will entirely depend on your specific controller/use case.
{{</tip>}}

- Various screwdrivers
- Needle Nose Pliers (can make wire removal easier)
- Button Popout tool (optional, but it makes it way easier to remove buttons). I recommend [this one](https://paradisearcadeshop.com/collections/accessories-parts/products/paradise-arcade-shop-button-popouts) from Paradise Arcade Shop.

### Materials {#brookmaterials}

{{<tip>}}
This list is just a generic suggestion, and is not an exhaustive list as it will entirely depend on your specific controller/use case.
{{</tip>}}

- 1x [LCC LED Spy 2.0 Board](http://www.luberryscc.com/product/led-spy-2-0)
- 1x [LCC LED Spy 20 Pin Breakout](http://www.luberryscc.com/product/20-pin-brook-generic-daughterboard-for-led-spy)
- [LCC 5050 LED Modules](http://www.luberryscc.com/product/lcc-5050-rgb-led-module) or other neopixel/ws2812 led options that supply 2x LEDs per actuator. (optional) [^3]
- LED Cables, number will vary based on the number of buttons in your controller. Some options below.
  - [Paradise Kaimana J2 3" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-3-wire-harness) (optional) [^3]
  - [Paradise Kaimana J2 6" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-6-wire-harness) (optional) [^3]
  - [Paradise Kaimana J2 8.5" Harness](https://paradisearcadeshop.com/products/paradise-kaimana-j2-85-wire-harness) (optional) 9" also works [^3]
- 1x usb Passthrough, can be neutrik, type c or whatever
  - You can wire the charge circut to the same usb port as the data port, but if you wish to have a separate port you can use another if you wish
- 24mm or 30mm Transparent/Clear buttons depending on your case and layout [^3]
- Way to fasten LEDS [^3]
  - Sanwa Buttons - the best way to fasten sanwa buttons is the [LCC Sanwa LED Module Mounting Clip](https://www.luberryscc.com/product/sanwa-led-module-mounting-clip)
  - Seimitsu Buttons - the LCC modules fit snugly around the seimitsu switch so no mounting mechanism is required.
  - Crowns/Gamerfingers/Other - a couple small dabs of hot glue are your friend in this instance as i have yet to make a reliable clip design for my modules

### Daughterboard Pinout {#brookdbpin}

{{< brookpinout >}}

### LED Spy Board Installation {#brookspyinstall}

Now the `LED Spy Board` can be installed.

1. First find a suitable spot to mount your board in your case, make sure the included 4 pin JST PH cable can reach the `LED Spy` board and your brook board, or you may need to get a longer one/extend the existing one.
2. Plug the included 4 pin JST cable into the port labelled JDB1 on the LED Spy PCB as pictured below.
   ![extcable](/images/install/jdb1.jpg)

3. Next you must mount the PCB. You can use some 3mm adhesive standoffs which you can pick up from our friends over at Bit Bang Gaming [here](https://bitbanggaming.com/collections/pcb-feet/products/snap-in-adhesive-pcb-feet-3mm-hole-4-pack).
4. Next plug the daughterboard into the Brook PCB or other generic PCB as shown, if you have more buttons than a standard brook pcb you can use the `EX0-EX10` pins of the board as shown in the [pinout](#brookdbpin).
   ![brookdb](/images/install/brook/daughterboard-module.png)

5. Now plug the LED Spy board into the Brook board, and use the 4 pin JST that came with the daughterboard to connect JB5 with the Touchpad/L3/R3 header on the board as shown below.
   ![brookinstall](/images/install/brook/daughterboard-install.jpg)

6. Now you can plug in a standard Brook 20 Pin Harness into `JB9` and the 4 pin Touchpad/L3/R3 harness into `JB4` as shown below.
   ![brookharness](/images/install/brook/daughterboard-harness.jpg)

7. Now connect the LED Spy Board to a usb passthrough either through the 5 pin `JUSB1` or the micro USB Port labeled `JUSB2`
   ![ls2](/images/led-spy-2.png)

### LED Installation {#brookledinstall}

The next step is to install the LED Modules, this guide will assume you are using the `LCC 5050 LED Modules`.

2. Position all of your modules so that they don't interfere with eachother. you may have to rotate the buttons some to get them all to fit. (its a bit of a puzzle).
   ![leds](/images/install/leds.jpg)
3. Now either secure the modules with some hot glue, or my LED clips (for sanwas) making sure the wires quick disconnects will still plug in.
4. Next we need to connect the leds. There are two different connectors on the leds. The 3 pin is the input, and the 4 pin is the output. Route your cables as you see fit as you can always remap the positon in software, however you want the input to be pretty close to the main pcb. You can see an example of how I wired my build below.
   ![wiring](/images/install/bbg/layout.jpg)
5. Once the leds are wired, reconnect all of the quick disconnects to the correct buttons.
6. Now plug the first led in the chain into the `JLED1` port of the LED Spy board.
7. Now, make sure you install the included jumper on the `LED 5V` pins to allow passthrough usb power for the leds.
8. Now once you reassemble your controller you may proceed to the [LED Spy Designer]({{<ref "/docs/designer">}}) documentation and follow the instructions there to set up your lighting, or use input display

## Bit Bang Gaming

{{<tip>}}
This guide assumes you have already installed your Bit Bang Gaming main board, if you have not done that yet, you will want to follow their [extensive resources](https://bitbanggaming.com/pages/resources).
{{</tip>}}

Since these boards are meant to go in custom builds, these instructions will be much more generic than the guides for full controller addons.

### Tools {#bbgtools}

{{<tip>}}
This list is just a generic suggestion, and is not an exhaustive list as it will entirely depend on your specific controller/use case.
{{</tip>}}

- Various screwdrivers
- Cordless Drill [^5] [^6]
- Various Drill Bits [^5] [^6]
- Center Punch, [this one](https://www.amazon.com/gp/product/B008DXYOLC/) is what i use. [^5] [^6]
- Safety Glasses (for use when drilling)
- Needle Nose Pliers (can make wire removal easier)
- Button Popout tool (optional, but it makes it way easier to remove buttons). I recommend [this one](https://paradisearcadeshop.com/collections/accessories-parts/products/paradise-arcade-shop-button-popouts) from Paradise Arcade Shop.

### Materials {#bbgmaterials}

{{<tip>}}
This list is just a generic suggestion, and is not an exhaustive list as it will entirely depend on your specific controller/use case.
{{</tip>}}

- 1x [LCC LED Spy 2.0 Board](http://www.luberryscc.com/product/led-spy-2-0)
- [LCC 5050 LED Modules](http://www.luberryscc.com/product/lcc-5050-rgb-led-module) or other neopixel/ws2812 led options that supply 2x LEDs per actuator. (optional) [^3]
- LED Cables, number will vary based on the number of buttons in your controller. Some options below.
  - [Paradise Kaimana J2 3" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-3-wire-harness) (optional) [^3]
  - [Paradise Kaimana J2 6" Harness](https://paradisearcadeshop.com/home/products/paradise-arcade-exclusive-products/paradise-kaimana-led-controller-and-accessories/1721_paradise-kaimana-j2-6-wire-harness) (optional) [^3]
  - [Paradise Kaimana J2 8.5" Harness](https://paradisearcadeshop.com/products/paradise-kaimana-j2-85-wire-harness) (optional) 9" also works [^3]
- 1x or 2x Usb Passthrough, can be neutrik, type c or whatever
  - You can wire the charge circut to the same usb port as the data port, but if you wish to have a separate port you can use another if you wish
- 1x [LCC LED SPY Battery Bank Wiring Kit (With Neutrik Switch Addon)](http://www.luberryscc.com/product/led-spy-battery-bank-wiring-kit) (optional) [^3]
- 1x always on [^4] battery bank, my recommendations are below.
  - [Voltaic Systems V50](https://voltaicsystems.com/v50/) (12800mAh)
  - [Voltaic Systems V25](https://voltaicsystems.com/v25/) (6400mAh)
- 1x USB power cable to run your battery bank and connect to your charge port of choice.
- 24mm or 30mm Transparent/Clear buttons depending on your case and layout [^3]
- Way to fasten LEDS [^3]
  - Sanwa Buttons - the best way to fasten sanwa buttons is the [LCC Sanwa LED Module Mounting Clip](https://www.luberryscc.com/product/sanwa-led-module-mounting-clip)
  - Seimitsu Buttons - the LCC modules fit snugly around the seimitsu switch so no mounting mechanism is required.
  - Crowns/Gamerfingers/Other - a couple small dabs of hot glue are your friend in this instance as i have yet to make a reliable clip design for my modules
- Masking/painters tape used to protect the paint on the surface during cutting [^5] [^6]
- 3M VHB (Very High Bond) Tape (for mounting the battery bank) [^3]

### Battery Bank Installation {#bbgbatinstall}

{{<tip>}}
If you are not installing LED's you may skip thist step, and move on to [LED Spy Board Installation](#bbgspyinstall)
{{</tip>}}

1. First you will need to use some VHB tape to secure the battery similar to the figure below. you can use more than pictured to secure it better.
   {{<tip>}}
   Make sure to clean both the battery and inside of the case with some sort of degreaser, or isopropyl alcohol to promote better adhesion
   {{</tip>}}
   ![batterytape](/images/install/sbx/battery.jpg)
2. Peel the backing from the tape on the battery mount the battery in a position where you will still have access to the charge port and one output port for wiring.
3. Next wire the battery harness to the power switch. If you purchased the wiring kit you should have received a cable similar to the one below (cable lengths may be different), and also if you elected for the neutrik switch port you should have a switch mount also pictured below.
   ![batterycable](/images/install/battery-harness-with-neutrik.jpg)
4. Now we plug the USB-A end of the harness into any of the available usb a ports, an example is pictured below.
   ![batteryusba](/images/install/bbg/battery-usba.jpg)
5. Assuming you have the voltaic battery. Plug in the charge cable type c port into the battery bank, and the other end into your usb passthrough.

### LED Spy Board Installation {#bbgspyinstall}

Now the `LED Spy Board` can be installed.

1. First find a suitable spot to mount your board in your case, make sure the included 4 pin JST PH cable can reach the `LED Spy` board and your `Bit Bang Gaming` board, or you may need to get a longer one/extend the existing one. I happened to mount it to my battery bank and extend the cable due to case constraints.
2. Plug the included 4 pin JST cable into the port labelled JEXT1 on the LED Spy PCB as pictured below.
   ![extcable](/images/install/led-spy-ext-cable.jpg)

3. Next you must mount the PCB. I have mounted it to the battery bank with some standoffs, superglue and screws because it is what i had on hand when writing this guide, however you can also use 3mm adhesive standoffs which you can pick up from our friends over at Bit Bang Gaming [here](https://bitbanggaming.com/collections/pcb-feet/products/snap-in-adhesive-pcb-feet-3mm-hole-4-pack).
   ![layout](/images/install/bbg/layout.jpg)

4. Now plug the LED Spy board into the Bit Bang PCB as shown below.
   ![bbgplug](/images/install/bbg/plug.jpg)

5. Now connect the LED Spy Board to a usb passthrough either through the 5 pin `JUSB1` or the micro USB Port labeled `JUSB2`
   ![ls2](/images/led-spy-2.png)

### LED Installation {#bbgledinstall}

The next step is to install the LED Modules, this guide will assume you are using the `LCC 5050 LED Modules`.

1. First take the 4 pin connector on the power harness connected to the battery and plug it into the port labelled `JPWR1`

2. Position all of your modules so that they don't interfere with eachother. you may have to rotate the buttons some to get them all to fit. (its a bit of a puzzle).
   ![leds](/images/install/leds.jpg)
3. Now either secure the modules with some hot glue, or my LED clips (for sanwas) making sure the wires quick disconnects will still plug in.
4. Next we need to connect the leds. There are two different connectors on the leds. The 3 pin is the input, and the 4 pin is the output. Route your cables as you see fit as you can always remap the positon in software, however you want the input to be pretty close to the main pcb. You can see an example of how I wired my build below.
   ![wiring](/images/install/bbg/layout.jpg)
5. Once the leds are wired, reconnect all of the quick disconnects to the correct buttons.
6. Now plug the first led in the chain into the `JLED1` port of the LED Spy board.
7. Now once you reassemble your controller you may proceed to the [LED Spy Designer]({{<ref "/docs/designer">}}) documentation and follow the instructions there to set up your lighting, or use input display

[^1]: Only if you wish to install LEDS, required for power switch installation
[^2]: These are good if you wish to install a rectangular power switch (to match the others on the controller), but are not required as everything can be accomplished with a file and patience
[^3]: Only needed if you wish to install leds
[^4]: Always on battery banks are recommended, as there is not enough startup current, and some of the smarter battery banks won't turn on, or will shut off after some time
[^5]: If your case does not have provisions for an extra usb port
[^6]: If your case does not have provisions for a power switch
