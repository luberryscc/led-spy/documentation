---
title: "LED Spy Installation Docs"
weight: 21
---

Welcome to the LED Spy installation guide! This guide shows you how to install the LED Spy 2.0 modboard in your controller.

{{< button "./before" "Get Started" >}}
